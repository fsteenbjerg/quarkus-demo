package dk.stonemountain.demo.rest;

import io.quarkus.test.junit.SubstrateTest;

@SubstrateTest
public class NativeEnvironmentResourceIT extends EnvironmentResourceTest {

    // Execute the same tests but in native mode.
}