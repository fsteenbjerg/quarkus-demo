package dk.stonemountain.demo.rest;

import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.containsString;

@QuarkusTest
public class EnvironmentResourceTest {
    @Test
    public void testEndpoint() {
        given()
          .when().get("/service/environment")
          .then()
             .statusCode(200)
             .body(containsString("hostname"));
    }

}