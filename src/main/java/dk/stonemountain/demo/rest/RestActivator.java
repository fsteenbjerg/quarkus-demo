package dk.stonemountain.demo.rest;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("/service")
public class RestActivator extends Application {
}