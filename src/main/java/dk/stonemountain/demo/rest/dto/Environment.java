package dk.stonemountain.demo.rest.dto;

import java.util.Map;

public class Environment {
    private Map<String, String> variables;
    private String hostname;

    public Environment() {
    }

    public Environment(Map<String, String> variables, String hostname) {
        this.variables = variables;
        this.hostname = hostname;
    }

    public Map<String, String> getVariables() {
        return variables;
    }

    public void setVariables(Map<String, String> variables) {
        this.variables = variables;
    }

    public String getHostname() {
        return hostname;
    }

    public void setHostname(String hostname) {
        this.hostname = hostname;
    }
}