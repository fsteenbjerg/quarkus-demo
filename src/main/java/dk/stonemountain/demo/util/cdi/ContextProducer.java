package dk.stonemountain.demo.util.cdi;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Map;

import javax.enterprise.inject.Produces;

public class ContextProducer {

    @Produces
    public Map<String, String> getEnvironmentVariables() {
        return System.getenv();
    }

    @Produces
    @Hostname
    public String getHostname() throws UnknownHostException {
        return InetAddress.getLocalHost().getHostName();
    }
}