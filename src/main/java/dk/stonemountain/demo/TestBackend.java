package dk.stonemountain.demo;

import javax.enterprise.context.ApplicationScoped;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@ApplicationScoped
public class TestBackend {
    private static Logger logger = LoggerFactory.getLogger(TestBackend.class);

    public String getHello() {
        logger.info("backend invoked");
        return "hello";
    }
}