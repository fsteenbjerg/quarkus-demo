
package dk.stonemountain.demo;

import java.util.Map;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.eclipse.microprofile.metrics.annotation.Timed;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import dk.stonemountain.demo.rest.dto.Environment;
import dk.stonemountain.demo.util.cdi.Hostname;

@Path("/environment")
@Timed(description = "environment resource timed", displayName = "environment")
public class EnvironmentResource {
    private static Logger logger = LoggerFactory.getLogger(EnvironmentResource.class);
    
    @Inject TestBackend tester;
    @Inject @Hostname String hostname;
    @Inject Map<String, String> env;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Environment getEnvironment() {
        logger.info("get environment has been invoked");
        return new Environment(env, hostname);
    }
}