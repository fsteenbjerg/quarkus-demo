# quarkus-demo
A small project created in vs code and by use of maven. The project contains a jax-rs resource and some cdi producers just to show that it works. The docker can run as a native docker image.

## vs code extentions installed
* Launch VS Code Quick Open (Ctrl+P)
* ext install vscjava.vscode-java-pack
* ext install vscjava.vscode-java-debug
* ext install redhat.vscode-quarkus

## create a quarkus maven project
* Launch command prompt in vs code by entering Ctrl-Shift-p:
* enter command 'quarkus: Generate a maven project'
* see link https://developers.redhat.com/blog/2019/09/23/how-the-new-quarkus-extension-for-visual-studio-code-improves-the-development-experience/

## install graalvm
* set the GRAALVM_HOME environment variable in .bashrc and execute these commands
* pushd  $GRAALVM_HOME
* cd bin
* ./gu install native-image
* popd

## make sure that we are using java 8
* export JAVA_HOME=/usr/lib/jvm/java-1.8.0-openjdk-amd64
* export PATH=/usr/lib/jvm/java-1.8.0-openjdk-amd64/bin:$PATH

## setting up java in vs code
* add java.home to settings.json (remember ',' at the end of the line above)
* "java.home": "/usr/lib/jvm/java-8-openjdk-amd64"
* by ctrl-shift p and enter "java: Configure java runtime"
* select "user settings" and then select "edit in settings.json"

## compile to native
* mvn -f pom.xml -Pnative clean package

## execute native
* target/quarkus-demo-1.0.0-SNAPSHOT-runner 

## run docker image with native
* docker build -f src/main/docker/Dockerfile.native -t quarkus-demo/v1 .
* docker run -i --rm -p8080:8080 quarkus-demo/v1 

## push stuff to bitbucket.com
* git remote add origin https://fsteenbjerg@bitbucket.org/fsteenbjerg/quarkus-demo.git
* git commit -m "initial release"
* git push -u origin master

## install on openshift online
* get login token via openshift console: https://console-openshift-console.apps.ca-central-1.starter.openshift-online.com/deploy-image?preselected-ns=fin-cluster (in the menu shown when using the button with your user name)
* oc login --token=ZiZUe2Kja9El4GtLBjy-X6i27t-GQNzb1-s3CcQwvn8 --server=https://api.ca-central-1.starter.openshift-online.com:6443 (use the token retrieved in the step above)
* Create bc: oc new-build quay.io/quarkus/ubi-quarkus-native-binary-s2i:19.2.1 --binary --name=quarkus-demo -l app=quarkus-demo
* Run build: oc start-build quarkus-demo --from-file=target/quarkus-demo-1.0.0-SNAPSHOT-runner --follow
* Create application: oc new-app quarkus-demo
* Create route: oc create -f src/main/openshift/route.yaml
* Get status: oc rollout status -w dc/quarkus-demo
* See the s2i image here: https://github.com/quarkusio/quarkus-images/tree/master/modules/quarkus-native-binary-s2i-scripts
* Check the released tags here: https://quay.io/repository/quarkus/ubi-quarkus-native-binary-s2i?tab=tags